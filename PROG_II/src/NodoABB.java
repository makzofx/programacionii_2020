public class NodoABB{ //clase interna
	
	//todo esto era private
	public Integer info;
	public NodoABB izq;
	public NodoABB der;

		
	public NodoABB(Integer info){
		this.info = info;
	}
	

	public boolean esHoja() {
		return izq==null && der==null;
	}

	
	
	@Override
	public String toString(){
		return info.toString();
	}
	
	

	
	
		}