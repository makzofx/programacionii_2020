public class ABB { // arbol binario
	private NodoABB raiz;

	public ABB() {
		this.raiz = null;
	}

	public boolean pertenece(int x) {
		return pertenece(x, this.raiz);
	}

	private static boolean pertenece(int x, NodoABB nodo) {
		if (nodo == null) {
			return false;
		}
		if (x < nodo.info) {
			return pertenece(x, nodo.izq);
		} else if (x > nodo.info) {
			return pertenece(x, nodo.der);
		}
		return true; // valor == x
	}

	public void agregar(Integer elem) {
		NodoABB nuevo = new NodoABB(elem);
		if (raiz == null)
			raiz = nuevo;
		else
			agregar(raiz, nuevo);
	}

	private void agregar(NodoABB padre, NodoABB nuevo) {
		if (padre.izq == null)
			padre.izq = nuevo;
		else if (padre.der == null)
			padre.der = nuevo;
		else
			// Decisión de implementación: genera el arbol a derecha
			agregar(padre.der, nuevo);
	}

	// mi pre Orden
	public String impPreOrden() {
		if (raiz == null)
			return "arbol vacio";
		else {
			return impPreOrden(raiz);
		}
	}

	// modificar primer padre == null
	// terminar aca
	private String impPreOrden(NodoABB padre) {
		String arbol = " " + padre.info.toString();
		// String arbol = "hola " ;
		if (padre.izq != null) {
			arbol = arbol + impPreOrden(padre.izq);
		}
		if (padre.der != null) {
			arbol = arbol + impPreOrden(padre.der);
		}
		return arbol;
	}

	public String impInorden() {
		if (this.raiz == null)
			return "arbol vacio";
		return impInorden(raiz);
	}

	private String impInorden(NodoABB raiz) {

		String ret = " ";

		if (raiz == null)
			return "nodoNull";

		if (raiz.izq != null)
			// ret+=raiz.izq.toString();
			return ret + impInorden(raiz.izq) + raiz.toString();

		if (raiz.der != null) {
			return ret + impInorden(raiz.der);

		}
		return ret;
	}

	public int sumarInternos() {
		if (this.raiz == null)
			return 0; // si el arbol esta vacio no suma nada. da 0
		return sumarInternos(raiz.izq) + sumarInternos(raiz.der);
	}

	private int sumarInternos(NodoABB padre) {
		if (padre == null) {
			return 0; // caso base porque el padre es null, devuelve 0
		}
		if (padre.esHoja()) {
			return 0; // caso base si es hoja
		} else {
			return padre.info + sumarInternos(padre.izq) + sumarInternos(padre.der);
		}
	}

	public boolean esABB() {
		if (raiz == null) // caso base - caso 0 - arbol vacio
			return true; // un arbol vacio tambien es ABB
		return esABB(raiz); // sino ejecuta el metodo recursivo
	}

	private boolean esABB(NodoABB padre) {
		boolean ret = true;
		boolean izq = true;
		boolean der = true;
		if (padre == null) {
			return false;
		}
		if (padre.izq != null) {
			return izq = izq && padre.info > minimo(padre.izq);
		}
		if (padre.der != null) {
			return der = der && padre.info < minimo(padre.der);
		} else
			return ret && izq && der;
	}

	public int maximo() {
		if (raiz != null)
			return maximo(raiz);
		else
			throw new RuntimeException("No esta definido");
	}

	private int maximo(NodoABB padre) {
		int maximo = padre.info;
		int maxIzq, maxDer;

		if (padre.izq != null) { // recorre el subarbol Izq y me da el mayor
			maxIzq = maximo(padre.izq);
			if (maxIzq > maximo) {
				maximo = maxIzq;
			}
		}
		if (padre.der != null) { // recorre el subarbol Izq y me da el mayor
			maxDer = maximo(padre.der);
			if (maxDer > maximo) {
				maximo = maxDer;
			}
		}
		return maximo;

	}

	public int minimo() {
		if (raiz != null)
			return minimo(raiz);
		else
			throw new RuntimeException("No esta definido");

	}

	private int minimo(NodoABB padre) {
		int minimo = padre.info;
		int minIzq, minDer;
		if (padre.izq != null) { // pregunto si padre izq no es null
			minIzq = minimo(padre.izq); // minimo de la izq va a ser el minimo desde padre.izq
			if (minIzq < minimo) { // si el minimo que encontre es menor al que tenia
				minimo = minIzq; // el nuevo int es el minimo
			}
		}
		if (padre.der != null) {
			minDer = minimo(padre.der);
			if (minDer < minimo) {
				minimo = minDer;
			}
		}
		return minimo;
	}

	public int ramaMasLarga() {
		if (raiz == null)
			return 0;
		else {
			return ramaMasLarga(raiz);
		}
	}

	private int ramaMasLarga(NodoABB padre) {
		if (padre.esHoja()) {
			return 1;
		}
		if (padre.izq != null & padre.der != null) {
			return Math.max(ramaMasLarga(padre.izq), ramaMasLarga(padre.der) + 1);
		}
		if (padre.izq != null) {
			return ramaMasLarga(padre.izq) + 1;
		}
		if (padre.der != null) {
			return ramaMasLarga(padre.der) + 1;
		} else
			return 0;
	}

	public NodoABB buscar(Integer elem) {
		return (raiz == null) ? null : buscar(raiz, elem);
	}

	private NodoABB buscar(NodoABB nodo, Integer elem) {
		if (nodo.info.equals(elem))
			return nodo;
		else {
			NodoABB izq = null;
			NodoABB der = null;
			if (nodo.izq != null)
				izq = buscar(nodo.izq, elem);
			if (nodo.der != null)
				der = buscar(nodo.der, elem);
			// Decisión de implementación: si esta en ambos lado, mostramos el izquierdo
			// primero
			if (izq != null)
				return izq;
			else
				return der;
		}
	}

	// sumar raiz y las hojas = 71
	// sumar todos los internos (101)
	// a suma de internos restarle la raiz y las hojas

	public int RaizMasHojas() {
		if (raiz == null)
			return 0;
		return raiz.info + RaizMasHojas(raiz);
	}

	private int RaizMasHojas(NodoABB padre) {
		int resul = 0;
		if (padre == null) { // caso base, nodo padre es nulo
			return 0; // sumo neutro
		}
		if (padre.esHoja()) { // que tengo que hacer si es hoja? sumarlo
			System.out.println("p" + padre);
			System.out.println("i" + padre.izq);
			System.out.println("d" + padre.der);
			return resul + padre.info;
		} else
			return resul + RaizMasHojas(padre.izq) + RaizMasHojas(padre.der);
	}


	public int nivel(int elem) {
		if (raiz == null)
			return -1;
		return nivel(elem, raiz, 1); // le paso el nivel, si es raiz le paso 1
	}

	private int nivel(int elem, NodoABB padre, int niv) {
		if (padre == null) {
			return -1;
		}
		if (padre.info == elem) {
			return niv;
		}

		if (padre.info > elem) {
			return nivel(elem, padre.izq, niv + 1);

		} else {
			return nivel(elem, padre.der, niv + 1);

		}
	}

	public int cantNodos_nivel(int niv) {
		return cantNodos_nivel(niv, raiz, 0);
	}

	private int cantNodos_nivel(int niv, NodoABB n, int niv_act) {
		if (n == null) {
			return 0;
		}
		if (niv == 0) {
			return 1;
		}
		return cantNodos_nivel(niv - 1, n.izq, niv_act + 1) + cantNodos_nivel(niv - 1, n.der, niv_act + 1);
	}
	// puede no ir niv_act tambien

	public int ramaMasCorta() {
		if (raiz == null) { // si raiz es null, la altura mas corta es 0 arbol vacio
			return 0;
		} else {
			return ramaMasCorta(raiz);
		}
	}

	private int ramaMasCorta(NodoABB padre) {
		// int masCorta = 0; //caso nodo sin hijos

		if (padre.esHoja()) {
			return 1; // si el padre es hoja, retorna 1-->caso base
		}

		if (padre.izq != null && padre.der != null) { // caso en el que el nodo tiene ambos hijos
			return Math.min(ramaMasCorta(padre.izq), ramaMasCorta(padre.der)) + 1;
			// retorna el minimo entre rama mas corta izquierda y rama mas corta derecha +1

		} else if (padre.izq != null) { // si solo tiene hijo izquierdo l
			return ramaMasCorta(padre.izq) + 1; // retorna la rama mas corta del izquierdo sumando 1

		} else { // si solo tiene hijo derecho
			return ramaMasCorta(padre.der) + 1; // retorna la rama mas corta del derecho sumando 1
		}
	}

	public void insertarABB(Integer laInfo) {
		NodoABB elnodo = new NodoABB(laInfo);
		if (raiz == null)
			raiz = elnodo;
		else
			insertarABB(raiz, elnodo);
	}

	private void insertarABB(NodoABB raiz, NodoABB elNodoNuevo) {
		if (raiz.info > elNodoNuevo.info) {
			if (raiz.izq == null) {
				raiz.izq = elNodoNuevo;
			} else {
				insertarABB(raiz.izq, elNodoNuevo);
			}
		} else if (raiz.info < elNodoNuevo.info) {
			if (raiz.der == null) {
				raiz.der = elNodoNuevo;
			} else {
				insertarABB(raiz.der, elNodoNuevo);
			}
		}
	}

	public int cantNodos() {
		if (raiz == null)
			return 0;
		return cantNodos(raiz);
		// return (raiz == null) ? 0 : cantNodos(raiz);
	}

	@SuppressWarnings("unused")
	private int cantNodos_profesor(NodoABB nodo) {
		int cantIzq = (nodo.izq == null) ? 0 : cantNodos(nodo.izq);
		int cantDer = (nodo.der == null) ? 0 : cantNodos(nodo.der);
		return 1 + cantIzq + cantDer;
	}

	private int cantNodos(NodoABB padre) {
		int cantIzq = 0;
		int cantDer = 0;

		if (padre.izq == null)
			cantIzq = 0;
		else {
			cantIzq = cantNodos(padre.izq);
		}

		if (padre.der == null)
			cantDer = 0;
		else {
			cantDer = cantNodos(padre.der);
		}
		return 1 + cantIzq + cantDer;
	}

	public static boolean estaEnElRango(int hasta, NodoABB padre) {
		return padre.info >= 1 && padre.info <= hasta;
	}

	public static boolean estaEnElRangoNum(int hasta, int padre) {
		return padre >= 1 && padre <= hasta;
	}

	public ABB abToabb(ABB arbolBinario) {
		ABB arbolRet = new ABB();
		if (arbolBinario.raiz == null) // alguna rama en null
			return arbolRet;
		return abToabb(arbolBinario.raiz, arbolRet);
	}

	private ABB abToabb(NodoABB nodo, ABB arbolRet) { // padre es nodo a agregar

		arbolRet.insertarABB(nodo.info); // inserta la raiz y despues recorre

		if (nodo.der != null)
			abToabb(nodo.der, arbolRet);

		if (nodo.izq != null)
			abToabb(nodo.izq, arbolRet);

		return arbolRet;
	}

	
	
	public int cantRango(int desde, int hasta) {
		if (raiz == null)
			return 0;
		return cantRango(desde, hasta, raiz);
	}

	private int cantRango(int desde, int hasta, NodoABB padre) {
		int cont = 0;
		if (padre == null) {
			return 0; // caso base si el padre es null

		}
		if (padre.info <= hasta && padre.info >= desde) {
			cont++;

		}
		return cont + cantRango(desde, hasta, padre.izq) + cantRango(desde, hasta, padre.der);

	}

	
	public boolean hojasConsecutivasK(int hasta) {
		if (raiz == null)
			return false;
		return (hojasConsecutivasK(hasta, raiz.izq));

	}

	private boolean hojasConsecutivasK(int hasta, NodoABB padre) {

		boolean ret = true;

		if (padre == null)
			return false;

		if (padre.esHoja() && estaEnElRango(hasta, padre))
			return true;

		if (padre.izq != null && padre.der == null)
			return ret && hojasConsecutivasK(hasta, padre.izq);

		if (padre.der != null && padre.izq == null)
			return ret && hojasConsecutivasK(hasta, padre.der);

		else
			return ret && (hojasConsecutivasK(hasta, padre.izq) && hojasConsecutivasK(hasta, padre.der));
	}

	public int sumarHojas() {
		if (raiz == null)
			return 0;
		else

			return sumarHojas(raiz);
	}

	private int sumarHojas(NodoABB padre) {
		if (padre == null)
			return 0;
		if (padre.esHoja())
			return padre.info + sumarHojas(padre.izq) + sumarHojas(padre.der);
		else
			return sumarHojas(padre.izq) + sumarHojas(padre.der);

	}

	
	
	public String hojasRango (Integer desde, Integer hasta) {
		if (raiz==null)
			return "{ }";
		else {
			return hojasRango (raiz, desde, hasta); 
		}
	}
	
	
	private String hojasRango (NodoABB nodo, Integer desde, Integer hasta) {
		String ret = "";
		
		if (nodo == null)
				return "";
		
		if (nodo.esHoja() && nodo.info>desde && nodo.info<hasta){
			return ret = ret + nodo.info.toString()+ ",";		
		}
		
		else
			return hojasRango(nodo.izq, desde, hasta) + hojasRango(nodo.der, desde, hasta) ;
		
		
	}
	
	
	
	public int cantHojas() {
		return cantHojas(raiz);
	}

	private int cantHojas(NodoABB n) {
		if (n == null) {
			return 0;
		}
		if (n.izq == null && n.der == null) {
			return 1;
		}
		return cantHojas(n.izq) + cantHojas(n.der);
	}

	
	
	
	
	
	
	public int sumarSubArboles() {
		if (raiz == null)
			return 0;
		return sumarSubArbol(raiz.der) + sumarSubArbol(raiz.izq);
	}

	private int sumarSubArbol(NodoABB padre) {
		if (padre == null)
			return 0;
		if (padre.izq != null && padre.der != null)
			return padre.info + sumarSubArbol(padre.izq) + sumarSubArbol(padre.der);
		if (padre.izq != null)
			return padre.info + sumarSubArbol(padre.izq);
		if (padre.der != null)
			return padre.info + sumarSubArbol(padre.der);
		else
			return padre.info + sumarSubArbol(padre.izq) + sumarSubArbol(padre.der);

	}

	public static boolean esPar(int elem) {
		return elem % 2 == 0;
	}

	public int sumarPares() {
		if (raiz == null)
			return 0;
		else
			return sumarPares(raiz);
	}

	private int sumarPares(NodoABB padre) {
		if (padre == null)
			return 0;
		if (esPar(padre.info) && padre.esHoja()) {
			return padre.info;
		}
		if (esPar(padre.info) && padre.izq != null && padre.der != null)
			return padre.info + sumarPares(padre.izq) + sumarPares(padre.der);
		if (esPar(padre.info) && padre.izq != null)
			return padre.info + sumarPares(padre.izq);
		if (esPar(padre.info) && padre.der != null)
			return padre.info + sumarPares(padre.der);
		else
			return 0 + sumarPares(padre.izq) + sumarPares(padre.der);

	}

	public boolean balanceado() {
		return balanceado(raiz);
	}

	private boolean balanceado(NodoABB nodo) {
		boolean ret = true;
		int altIzq = 0;
		int altDer = 0;
		if (nodo.izq != null) {
			altIzq = altura(nodo.izq);
			ret = ret && balanceado(nodo.izq);
		}
		if (nodo.der != null) {
			altDer = altura(nodo.der);
			ret = ret && balanceado(nodo.der);
		}
		ret = ret && Math.abs(altIzq - altDer) <= 1;
		return ret;
	}

	public int altura() {
		return (raiz == null) ? 0 : altura(raiz);
	}

	private int altura(NodoABB nodo) {
		int altIzq = (nodo.izq == null) ? 0 : altura(nodo.izq);
		int altDer = (nodo.der == null) ? 0 : altura(nodo.der);
		return 1 + Math.max(altIzq, altDer);
	}


	@Override
	public String toString() {
		return (raiz == null) ? "HardCoNullEmpty" : toString(raiz);
	}

	
	
	
	
	private String toString(NodoABB nodo) {
		String ret = nodo.info.toString() + "-";
		if (nodo.izq != null)
			ret = ret + toString(nodo.izq);
		if (nodo.der != null)
			ret = ret + toString(nodo.der);
		return ret;
	}

	public static void main(String[] args) {

		// Este es Arbol Binario de Busqueda ABB
		ABB ArbolBinarioDeBusqueda = new ABB();
		ArbolBinarioDeBusqueda.insertarABB(15);
		ArbolBinarioDeBusqueda.insertarABB(11);
		ArbolBinarioDeBusqueda.insertarABB(3);
		ArbolBinarioDeBusqueda.insertarABB(1);
		ArbolBinarioDeBusqueda.insertarABB(5);
		ArbolBinarioDeBusqueda.insertarABB(22);
		ArbolBinarioDeBusqueda.insertarABB(17);

		ArbolBinarioDeBusqueda.insertarABB(25);
		ArbolBinarioDeBusqueda.insertarABB(23);
		ArbolBinarioDeBusqueda.insertarABB(31);



		// Este es solo Arbol Binario
		ABB ArbolBinario = new ABB();
		ArbolBinario.agregar(12);
		ArbolBinario.agregar(39);
		ArbolBinario.agregar(45);
		ArbolBinario.agregar(40);
		ArbolBinario.agregar(5);
		ArbolBinario.agregar(3);
		ArbolBinario.agregar(10);
		ArbolBinario.agregar(18);

		// System.out.println(arb.toString());
		// System.out.println(ArbolBinario.impPreOrden());
		// System.out.println(ArbolBinarioDeBusqueda.impPreOrden());
		// System.out.println("la suma de los interno es:"+ arb.sumarInternos());
		// System.out.println(ArbolBinarioDeBusqueda.maximo());
		// System.out.println(ArbolBinarioDeBusqueda.minimo());
		// System.out.println(ArbolBinarioDeBusqueda.esABB());
		// System.out.println("la suma:"+ArbolBinarioDeBusqueda.RaizMasHojas());
		// System.out.println("desdeHasta:"+ArbolBinario.cantRango(0, 10));
		// int elem = 22;
		// System.out.println("elemento:"+elem);
		// System.out.println("Nivel="+ArbolBinarioDeBusqueda.nivel(elem));
		// System.out.println("cantNodos: "+ArbolBinarioDeBusqueda.cantNodos());
		// System.out.println(ArbolBinarioDeBusqueda.ramaMasCorta());
		// System.out.println(ArbolBinario.ramaMasCorta());

		// System.out.println(ArbolBinario.ramaMasLarga());
		// System.out.println(ArbolBinarioDeBusqueda.ramaMasLarga());

		// System.out.println(ArbolBinarioDeBusqueda.sumarSubArboles());
		// System.out.println("SumarPares: "+ArbolBinarioDeBusqueda.sumarPares());

		// System.out.println(estaEnElRangoNum(40, 39));

		// System.out.println(ArbolBinarioDeBusqueda.hojasConsecutivasK(10));

		// System.out.println("el
		// nuevoarbol"+ArbolBinario.abToabb(ArbolBinario).impPreOrden());

		//System.out.println(ArbolBinarioDeBusqueda.impPreOrden());
		System.out.println(ArbolBinarioDeBusqueda.hojasRango(4, 25));
	}

}