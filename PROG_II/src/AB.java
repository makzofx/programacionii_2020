public class AB { // arbol binario

	private Nodo raiz;

	public void agregar(int elem) {
		Nodo nuevo = new Nodo(elem);
		if (raiz == null)
			raiz = nuevo;
		else
			agregar(raiz, nuevo);
	}

	private void agregar(Nodo padre, Nodo nuevo) {
		if (padre.izq == null)
			padre.izq = nuevo;
		else if (padre.der == null)
			padre.der = nuevo;
		else
			// Decisión de implementación: genera el arbol a derecha
			agregar(padre.der, nuevo);
	}

	public Nodo buscar(int elem) {
		return (raiz == null) ? null : buscar(raiz, elem);
	}

	private Nodo buscar(Nodo nodo, int elem) {
		if (nodo.info == elem)
			return nodo;
		else {
			Nodo izq = null;
			Nodo der = null;
			if (nodo.izq != null)
				izq = buscar(nodo.izq, elem);
			if (nodo.der != null)
				der = buscar(nodo.der, elem);
			// Decisión de implementación: si esta en ambos lado, mostramos el izquierdo
			// primero
			if (izq != null)
				return izq;
			else
				return der;
		}
	}

	public int cantNodos() {
		return (raiz == null) ? 0 : cantNodos(raiz);
	}

	private int cantNodos(Nodo nodo) {
		int cantIzq = (nodo.izq == null) ? 0 : cantNodos(nodo.izq);
		int cantDer = (nodo.der == null) ? 0 : cantNodos(nodo.der);
		return 1 + cantIzq + cantDer;
	}

	public int altura() {
		return (raiz == null) ? 0 : altura(raiz);
	}

	private int altura(Nodo nodo) {
		int altIzq = (nodo.izq == null) ? 0 : altura(nodo.izq);
		int altDer = (nodo.der == null) ? 0 : altura(nodo.der);
		return 1 + Math.max(altIzq, altDer);
	}

}